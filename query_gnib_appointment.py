#Author : g.sabarinath91@gmail.com
import requests
from OpenSSL import crypto
import pem
import ssl
import certifi
from requests import RequestException


urlheaders = {
    'User-agent': 'script/python',
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip,deflate,br',
    'origin': 'null',
    'connection': 'keep-alive',
}


class GnibException(Exception):
    def __init__(self, optiontuple, errtype=None):
        self.Option = optiontuple
        self.ErrType = errtype
        super(GnibException, self).__init__(optiontuple, errtype)

    def __str__(self):
        return '{0} Not Found:It should be anyone item of following tuple {1}'.format(self.ErrType, self.Option)


class CategoryValueError(GnibException):
    def __init__(self):
        self.Option = ('Other', 'Study', 'Work')
        self.ErrType = 'Category'


class TypeValueError(GnibException):
    def __init__(self):
        self.Option = ('New', 'Renewal')
        self.ErrType = 'Type'


class GnibUrlGenerator:
    def __init__(self, date=None, category='', sbcategory='All', permit_type='', headers=urlheaders):
        self.url = 'https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/'
        self.page = ('openpage', '')
        self.date = date
        self.cat = category
        self.sbcat = sbcategory
        self.type = permit_type
        self.headers = headers
        self.CertCryptoObj = ssl.get_server_certificate((self.url.lstrip('https://').split('/')[0], 443))
        self.urlroot = "https://"+self.url.lstrip('https://').split('/')[0]
        self.CabundleObj = pem.parse_file(certifi.where())

    def validate_params(self):
        if self.cat not in ['Other', 'Study', 'Work']:
            raise CategoryValueError()
        if self.type not in ['New', 'Renewal']:
            raise TypeValueError()

    def check_cert_issuer_exists(self, certcryptoobj):
        certcryptoobj = crypto.load_certificate(crypto.FILETYPE_PEM, certcryptoobj)
        try:
            requests.get(self.urlroot)
            print('URL Access : Success')
        except requests.exceptions.SSLError:
            raise RequestException('certificate issuer {0} is not found in root CA bundle,So it may be due to missing i'
                                   'ntermediate CA chain'.format(certcryptoobj.get_issuer().CN))

    def add_interca_cert(self):
        with open('SymantecClass3SecureServerCA-G4.pem', 'r') as intercacert:
            inter_ca = intercacert.read()
        with open(certifi.where(), 'a') as cabundle:
            cabundle.write('\n' + inter_ca)
            print("ca cert added")

    def getavailable_appointment(self):
        try:
            self.validate_params()
            self.check_cert_issuer_exists(self.CertCryptoObj)
            return requests.get(self.url + '(getApps4DT)?readform', params=(
                                self.page, ('dt', self.date), ('cat', self.cat), ('sbcat', self.sbcat),
                                ('typ', self.type)), headers=self.headers, verify=True)
        except (CategoryValueError, TypeValueError) as err:
            print(err)
        except RequestException as err:
            print(err, 'So adding IntermediateCA chain to CA store.')
            self.add_interca_cert()
            return self.getavailable_appointment()

    def getaarliest_appointment(self):
        self.validate_params()
        return requests.get(self.url + '(getAppsNear)?readform',
                            params=(self.page, ('cat', self.cat), ('sbcat', self.sbcat), ('typ', self.type)),
                            headers=self.headers, verify=True)

    def getdate_availability(self):
        self.validate_params()
        return requests.get(self.url + '(getApps4DTAvailability)?readform',
                            params=(self.page, ('cat', self.cat), ('sbcat', self.sbcat), ('typ', self.type)),
                            headers=self.headers, verify=True)


url = GnibUrlGenerator(date='15/08/2018', category='Work', permit_type='New')

# url = GnibUrlGenerator()

result = url.getavailable_appointment()
resultA = url.getdate_availability()
resultB = url.getaarliest_appointment()

print(result.url, '\n', result.headers, '\n', result.text, '\n', result.status_code)

print(
    '\n############################################################################################################'
    '#######\n')
print(resultA.url, '\n', resultA.headers,  '\n', resultA.text, '\n', resultA.status_code)
print(
    '\n###########################################################################################################'
    '########\n')
print(resultB.url, '\n', resultB.headers, '\n', resultB.text, '\n', resultB.status_code)
print(
    '\n###########################################################################################################'
    '########\n')
